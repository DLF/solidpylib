from solid import *
from math import tan, radians


class ScrewFlatHead:
    """
    A screw with countersunk, flat head.
    This model has no thread. It's used as cutting die for screw holes.

    :param d: diameter of the screw
    :param dh: diameter of the head
    :param ah: angle of the head
    :param l: length of the *complete* screw (including head)
    :param head_elongation: length of optional cylinder with diameter dh, placed on top of the head.
                     Useful since this model is mostly used as a cutting die. (default 0)
    :param segments: circle resolution $fn for OpenSCAD (default 30)
    :param screw_color: color of the screw (bolt and head)
    :param elongation_color: color of the elongation cylinder (if specified)

    The screw head is lying on z=0, the screw bolt is going downwards (-z).
    The screw is centered on x/y.
    The elongation (if specified), goes upwards (+z).

    DIN 963 metric screws have a head angle (ah) of 90°.

    DIN 963 metric screw head diameters:
     • M 2     3,8 mm
     • M 2,5   4,7 mm
     • M 3     5,6 mm
     • M 4     7,5 mm
     • M 5     9,2 mm
     • M 6     11 mm
     • M 8     14,5 mm
     • M 10    18 mm
     • M 12    22  mm
    """
    def __init__(
            self,
            d,
            dh,
            ah,
            l,
            head_elongation=0,
            segments=30,
            screw_color="DodgerBlue",
            elongation_color="Orchid"
    ):
        self.head_cylinder_height = (dh/2)/tan(radians(ah/2))
        self.l = l
        self.d = d
        self.dh = dh
        self.head_elongation = head_elongation
        self.segments = segments
        self.screw_color = screw_color
        self.elongation_color = elongation_color

    def __call__(self):
        screw = color(self.screw_color)(
            translate((0, 0, -self.l))(cylinder(d=self.d, h=self.l, segments=self.segments))
            +
            translate((0, 0, -self.head_cylinder_height))(
                cylinder(d1=0, d2=self.dh, h=self.head_cylinder_height, segments=self.segments)
            )
        )
        if self.head_elongation > 0:
            return screw + color(self.elongation_color)(
                translate((0, 0, -0.01))(
                    cylinder(d=self.dh, h=self.head_elongation, segments=self.segments)
                )
            )
        else:
            return screw


class ScrewFlatHeadM3(ScrewFlatHead):
    din_screw_diameter = 3
    din_head_diameter = 5.6

    def __init__(
        self,
        length,
        clearance=0.2,
        head_elongation=0,
        segments=30,
        screw_color="DodgerBlue",
        elongation_color="Orchid"
    ):
        ScrewFlatHead.__init__(
            self,
            d=self.din_screw_diameter + clearance,
            dh=self.din_head_diameter + clearance,
            ah=90,
            l=length,
            head_elongation=head_elongation,
            segments=segments,
            screw_color=screw_color,
            elongation_color=elongation_color
        )
