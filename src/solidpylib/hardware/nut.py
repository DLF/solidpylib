from solid import *
from math import sqrt


# // m2
# nut_size=4;         // DIN 934
# nut_height=1.6;     // DIN 934
# // m3
# nut_size=5.5 + 0.2;        // DIN 934
# nut_height=2.4 + 0.2;      // DIN 934


def _hexagon(d):
    ra = d/2 * 2/sqrt(3)
    return circle(r=ra, segments=6)


class Nut:
    def __init__(self, size, height):
        self.size = size
        self.height = height

    def __call__(self):
        return linear_extrude(self.height)(
            _hexagon(self.size)
        )


class NutM3(Nut):
    nut_size = 5.5
    nut_height = 2.4

    def __init__(self, clearance=0.2, height=None):
        if height is None:
            height = self.nut_height
        Nut.__init__(
            self,
            size=self.nut_size + clearance,
            height=height
        )
